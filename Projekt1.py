from gpiozero import LED, Button, PWMOutputDevice
from time import sleep
import random

green = LED(22)
yellow = LED(27)
red = LED(17)
button = Button(16, pull_up = True)
buzzer = PWMOutputDevice(12)
buzzer.frequency = 500
buzzer.value = 0.0

def buzz (frequency, period):
    buzzer.frequency = frequency
    buzzer.value = 0.5
    sleep (period/2)
    buzzer.value = 0.0
    sleep (period/2)


def switchLights (greenLight, yellowLight, redLight, sleepTime):
    if greenLight:
        green.on()
    else:
        green.off()

    if yellowLight:
        yellow.on()
    else:
        yellow.off()

    if redLight:
        red.on()
    else:
        red.off()

    sleep(sleepTime)

while True:
    switchLights (True, False, False, 1)
    button.wait_for_press()
    wait = random.randint(10,15)
    print ("Wait for %d seconds ..." % wait)
    switchLights (True, False, False, wait)
    switchLights (False, True, False, 1)
    switchLights (False, False, True, 0)
    for i in range(0,10):
        buzz(500, 1)
    switchLights(False, True, True, 1)
